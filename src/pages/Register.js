import React from 'react';
import {
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBInput,
  MDBIcon,
  MDBCheckbox
}
from 'mdb-react-ui-kit';

export default function Register() {
  return (
    <MDBContainer fluid>

      <MDBCard className='text-black m-5' style={{borderRadius: '25px'}}>
        <MDBCardBody>
          <MDBRow>
            <MDBCol md='10' lg='6' className='order-2 order-lg-1 d-flex flex-column align-items-center'>

              <p classNAme="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-5">Sign up</p>

              <div className="d-flex flex-row align-items-center ">
                <MDBIcon fas icon="user me-3" size='lg'/>
                <MDBInput label='Your Name' id='form1' type='text' className='w-100'
                placeholder="Full Name"/>
              </div>

              <div className="d-flex flex-row align-items-center">
                <MDBIcon fas icon="envelope me-3" size='lg'/>
                <MDBInput label='Your Email' id='form2' type='email'
                placeholder="Enter Email"/>
              </div>

              <div className="d-flex flex-row align-items-center">
                <MDBIcon fas icon="envelope me-3" size='lg'/>
                <MDBInput label='Mobile Number' id='form2' type='email'
                placeholder="099999999"/>
              </div>

              <div className="d-flex flex-row align-items-center">
                <MDBIcon fas icon="lock me-3" size='lg'/>
                <MDBInput label='Password' id='form3' type='password'
                placeholder="Password"/>
              </div>

              <div className="d-flex flex-row align-items-center">
                <MDBIcon fas icon="key me-3" size='lg'/>
                <MDBInput label='Repeat your password' id='form4' type='password'
                placeholder="Confirm Password"/>
              </div>


              <MDBBtn className='success' size='lg'>Register</MDBBtn>

            </MDBCol>

            <MDBCol md='10' lg='6' className='order-1 order-lg-2 d-flex align-items-center'>
              <MDBCardImage src='https://i.pinimg.com/originals/cf/9b/6e/cf9b6e912d834e67883083a1c3b12f7e.jpg' fluid/>
            </MDBCol>

          </MDBRow>
        </MDBCardBody>
      </MDBCard>

    </MDBContainer>
  );
}

